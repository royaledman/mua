<?php

namespace App\Http\Controllers\Web\Documents;

use App\Http\Controllers\Controller;
use App\Http\Resources\Web\DocumentTrueResource;
use App\Models\Department;
use App\Models\Document;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DocumentController extends Controller
{
    public function show(Department $department)
    {
        $dambo = User::where('department_id', $department->id);
        $userDocs = $dambo->with('docs')->get();
        $dido = null;
        if ($userDocs)
            foreach ($userDocs as $one) {
                $dido = $one->docs;
            }
//        dd($dido);
        return view('index', compact('userDocs', 'dido'));
    }

    public function send(Request $request)
    {
        $user = Auth::user();
        $this->validate($request, [
            'file' => 'required',
            'title' => 'required',
            'user_id' => 'required'
        ]);

        $docs = $request->file('file');
        $docName = $docs->getClientOriginalName();
        $docPath = public_path('uploads/', $docName);
        $docs->move($docPath, $docName);

        $result = Document::create([
            'file' => 'uploads/' . $docName,
            'title' => $request->title,
            'user_id' => $user->id
        ]);
        return new DocumentTrueResource($result);
    }

}
