<?php

namespace App\Http\Controllers\Web\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Web\Auth\LoginRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function show()
    {
        return view('auth.login');
    }

    function login(LoginRequest $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            // Authentication passed...
            return view('auth.cabinet');
        }else{
            return redirect()->back();
        }
//        $credentials = $request->validated();
//        if (Auth::attempt($credentials)) {
//            $request->session()->regenerate();
//            return view('auth.cabinet');
//        }else
//            return redirect()->back();
//        return view('auth.login');
    }



}
