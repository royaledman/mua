<?php

namespace App\Http\Controllers\Web\Department;

use App\Http\Controllers\Controller;
use App\Models\Department;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{
    public function allDeps()
    {
        $depars = Department::get();
        return view('deps', compact('depars'));
    }
}
