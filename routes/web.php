<?php

use App\Http\Controllers\Web\Auth\LoginController;
use App\Http\Controllers\Web\Auth\LogoutController;
use App\Http\Controllers\Web\Department\DepartmentController;
use App\Http\Controllers\Web\Documents\DocumentController;
use App\Http\Controllers\Web\User\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware('auth:sanctum')->group(function (){
    Route::post('/upload', [DocumentController::class, 'send'])->name('upload');
    Route::get('/page', [UserController::class, 'cabinet'])->name('cabinet');
    Route::get('logout', [LogoutController::class, 'logout']);

});

Route::get('/doc/{department}', [DocumentController::class, 'show'])->name('each');
Route::get('/depars', [DepartmentController::class, 'allDeps']);
Route::get('/auth/login', [LoginController::class, 'show'])->name('loginPage');

Route::post('/auth/enter', [LoginController::class, 'login'])->name('login');


