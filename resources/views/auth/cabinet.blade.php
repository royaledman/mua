<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <style>
    .login{
        justify-content: center;
        align-items: center;
        flex-direction: column;
        display: flex;

        margin-top: 10px;
    }
    .btn{
        margin: 0 auto;
        display: block;
        margin-top: 10px;
        color: black;
        margin-bottom: 20px;
    }
    .log2{
        display: flex;
        flex-direction: column;
        justify-content: space-evenly;

        margin: 20px;
    }
    .log3{
        margin-top: 20px;
        justify-content: space-between;
    }
    .log4{
        margin-top: 30px;
    }
    </style>
</head>
<header class="p-3 bg-white text-dark">
    <div class="container">
        <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
            <ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
                <li><a href="#" class="nav-link px-2 text-dark">Home</a></li>
                <li><a href="#" class="nav-link px-2 text-dark">Features</a></li>


            </ul>


        </div>
    </div>
</header>
<body>
<div class="login bg-black">

{{--    <div style="color: white; justify-content: center">--}}
{{--        <h1>Вы зашли</h1>--}}
{{--    </div>--}}
    <div class="log4" style="color: white; justify-content: center">
        <h4>Загрузите файл</h4>
    </div>
    <div class="">

        <form action="{{ route('upload') }}" method="POST" enctype='multipart/form-data'>
            @csrf
            <div class="log2">
                <input name="title" type="text" id="title">
                <input type="hidden" name="user_id" id="user_id"
                       value="{{\Illuminate\Support\Facades\Auth::user() ? \Illuminate\Support\Facades\Auth::user()->id:null}}">
                <input  class="log3" name="file" id="file" type="file">
            </div>
            <button class="btn bg-white load-btn" type="submit"> Загрузить </button>
        </form>
    </div>

</div>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
    const loadBtn = document.querySelector('.load-btn');

    loadBtn.addEventListener('click', (e) => {
        e.preventDefault();
        Swal.fire({
            icon: 'success',
            title: 'Успешно!',
            text: 'Данные были отправлены',
        }).then((isConfirm) => {
            const formData = new FormData();
            formData.append('file', document.getElementById('file').files[0])
            formData.append('user_id',document.getElementById('user_id').value)
            formData.append('title',document.getElementById('title').value)
            axios.post('/upload',formData, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                },
            })
        })
    })
</script>

</body>
</html>
