<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <!-- CSS only -->
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <style>

.department{
    margin-bottom: 100px;
    padding: 20px;
    justify-content: center;
    display: flex;
    color: white;
    text-decoration: none;
}

    </style>
</head>
<body>

<header class="p-3 bg-white text-dark">
    <div class="container">
        <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
            <ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
                <li><a href="#" class="nav-link px-2 text-dark">Home</a></li>
                <li><a href="#" class="nav-link px-2 text-dark">Features</a></li>
                <li>
                    <div class="text-end">
                        <a href="{{ route('loginPage') }}">
                            <button type="button" class="btn btn-outline-dark me-2">Login</button>
                        </a>
                    </div>
                </li>

            </ul>


        </div>
    </div>
</header>

<div class="column m-2 p-4">
    <div class="row justify-content-center">

        @foreach($depars as $one)
            {{--            <a href="{{ $document->file }}">document {{$document->id}}</a>--}}
            <a class="department bg-dark" href="{{ route('each', $one->id) }}">
                {{--                @dd($one)--}}
                {{ $one->title }}
            </a>
        @endforeach

    </div>
</div>

<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ"
        crossorigin="anonymous"></script>

</body>
</html>
